var bgPage = chrome.extension.getBackgroundPage();
var _gaq = bgPage._gaq;
var tracking = function() {
	var ga = document.createElement('script');
	ga.type = 'text/javascript';
	ga.async = true;
	ga.src = 'https://ssl.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(ga, s);
}
angular.module('FreedomCalc', []).
directive('contenteditable', function() {
	return {
		restrict: 'A', // only activate on element attribute
		require: '?ngModel', // get a hold of NgModelController
		link: function(scope, element, attrs, ngModel) {
			if (!ngModel) return; // do nothing if no ng-model

			// Specify how UI should be updated
			ngModel.$render = function() {
				element.text(ngModel.$viewValue || '');
			};

			// Listen for change events to enable binding
			element.on('blur keyup change', function() {
				scope.$apply(read);
			});

			function read() {
				var html = element.text();
				html = html.replace(/<div><br><\/div>/g, '');
				html = html.replace(/<br>/g, '');
				ngModel.$setViewValue(html);
			}
		}
	};
});
$('html').bind('keypress', function(e) {
	if (e.keyCode == 13 && e.altKey==false) {
		return false;
	}
});
$(function(){
	tracking();
})
var FreedomCalcController = function($scope) {
	_gaq.push(['_trackEvent', 'Opened', 'Opened']);
	$scope.content = {};
	$scope.contents = [];
	chrome.storage.sync.get('contents', function(result) {
		if (result.contents) {
			$scope.contents = result.contents;
		}
		document.getElementById('editable').focus();
	});

	$scope.evaluate = function(evt, content) {
		if (evt.keyCode == 13 && content.expr != '' && evt.altKey==false) {
			if(content.expr.charAt(0)=='+'||content.expr.charAt(0)=='-'||content.expr.charAt(0)=='*' || content.expr.charAt(0)=='/' || content.expr.charAt(0)=='%'){
				content.expr="ans"+content.expr;
			}
			bgPage.evaluate(content.expr, function(err, result) {
				if (err) {
					content.result = err;
				} else {
					$scope.contents.unshift({
						"expr": content.expr,
						"result": result
					});
					$scope.content = {};
					chrome.storage.sync.set({
						'contents': $scope.contents
					}, function() {});
				}
			})
			return true;
		}

	}
	$scope.evaluatePast = function(evt, cont) {
		if (evt.keyCode == 13 && cont.expr != '' && evt.altKey==false) {
			bgPage.evaluate(cont.expr, function(err, result) {
				if (err) {
					cont.result = err;
				} else {
					cont.result = result;
					chrome.storage.sync.set({
						'contents': $scope.contents
					}, function() {});
				}
			})
			return true;
		}
	}

	$scope.clearBoard = function() {
		$scope.content = {};
		$scope.contents = [];
		bgPage.scope={};
		bgPage.setScope();
		chrome.storage.sync.set({
			'contents': []
		}, function() {});
	}
	chrome.storage.onChanged.addListener(function(changes, namespace) {
		for (key in changes) {
			if (key == 'contents') {
				$scope.contents = changes[key].newValue;
				$scope.$apply();
			}
		}
	});
}

