var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UUA-44437920-1']);
_gaq.push(['_trackPageview']);
var scope = {};
chrome.storage.sync.get('scope', function(result) {
	if (result.scope) {
		scope = result.scope;
	} else {
		chrome.storage.sync.set({
			'scope': scope
		}, function() {

		});
	}
});
var context = "selection";
var title = "Freedom Calc";
var id = chrome.contextMenus.create({
	"id": "freedomcalc",
	"title": title,
	"contexts": [context],
	"onclick": function(info, tab) {
		chrome.storage.sync.get('contents', function(result) {
			var contents = result.contents;
			if (!contents) {
				contents = [];
			}
			evaluate(info.selectionText, function(err, result) {
				if (err) {
					chrome.notifications.create("ToDoToDo", {
						type: "basic",
						title: 'FreedomCalc',
						message: err,
						iconUrl: '/img/icon.ico'
					}, function() {

					});
				} else {
					contents.unshift({
						"expr": info.selectionText,
						"result": result
					})
					chrome.storage.sync.set({
						'contents': contents
					}, function() {
						chrome.notifications.create("ToDoToDo", {
							type: "basic",
							title: 'FreedomCalc',
							message: info.selectionText + "=" + result,
							iconUrl: '/img/icon.ico'
						}, function() {

						});
					})
				}
			});
		});
	}
});
chrome.storage.onChanged.addListener(function(changes, namespace) {
	for (key in changes) {
		if (key == 'scope') {
			scope = changes[key].newValue;
		}
	}
});
var setScope = function() {
	chrome.storage.sync.set({
		'scope': scope
	}, function() {});
}

var evaluate = function(expr, callback) {
	try {
		math.eval(expr, scope);
		result = scope.ans;
		if (typeof result === 'object') {
			if (result.value) {
				result = result.value
				if (result.unit) {
					result = result + " " + result.unit.name;
				}
			}else if (result._data) {
				result = result._data;
			}else{
				result=''
			}
		}
		if (typeof result === 'function') {
			result = '';
		}
		setScope();
		callback(undefined, result);
	} catch (err) {
		callback(err.message);
	}

}