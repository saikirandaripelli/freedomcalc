chrome.runtime.getBackgroundPage(function(backgroundPage) {
	bgPage = backgroundPage;
})
var tracking = function() {
	var service = analytics.getService('FreedomCalc');
	var tracker = service.getTracker('UA-44437920-1');
	tracker.sendAppView('Opened');
}
angular.module('FreedomCalc', []).
directive('contenteditable', function() {
	return {
		restrict: 'A', // only activate on element attribute
		require: '?ngModel', // get a hold of NgModelController
		link: function(scope, element, attrs, ngModel) {
			if (!ngModel) return; // do nothing if no ng-model

			// Specify how UI should be updated
			ngModel.$render = function() {
				element.text(ngModel.$viewValue || '');
			};

			// Listen for change events to enable binding
			element.on('blur keyup change', function() {
				scope.$apply(read);
			});

			function read() {
				var html = element.text();
				html = html.replace(/<div><br><\/div>/g, '');
				html = html.replace(/<br>/g, '');
				ngModel.$setViewValue(html);
			}
		}
	};
});

var context = "selection";
var title = "Freedom Calc";
var id = chrome.contextMenus.create({
	"id": "freedomcalculator",
	"title": title,
	"contexts": [context]
});
chrome.contextMenus.onClicked.addListener(function(info) {
	chrome.storage.sync.get('contents', function(result) {
		var contents = result.contents;
		if (!contents) {
			contents = [];
		}
		bgPage.evaluate(info.selectionText, function(err, result) {
			if (err) {
				chrome.notifications.create("ToDoToDo", {
					type: "basic",
					title: 'FreedomCalc',
					message: err,
					iconUrl: '/img/icon.ico'
				}, function() {

				});
			} else {
				contents.unshift({
					"expr": info.selectionText,
					"result": result
				})
				chrome.storage.sync.set({
					'contents': contents
				}, function() {
					chrome.notifications.create("ToDoToDo", {
						type: "basic",
						title: 'FreedomCalc',
						message: info.selectionText + "=" + result,
						iconUrl: '/img/icon.ico'
					}, function() {

					});
				})
			}
		});
	});
});
var blockKey = function(e) {
	if (e.keyCode == 13 && e.altKey == false) {
		return false;
	}
}
var FreedomCalcController = function($scope) {
	tracking();

	$scope.content = {};
	$scope.contents = [];
	chrome.storage.sync.get('contents', function(result) {
		if (result.contents) {
			$scope.contents = result.contents;
		}
		document.getElementById('editable').focus();
	});

	$scope.evaluate = function(evt, content) {
		if (evt.keyCode == 13 && content.expr != '' && evt.altKey == false) {
			if (content.expr.charAt(0) == '+' || content.expr.charAt(0) == '-' || content.expr.charAt(0) == '*' || content.expr.charAt(0) == '/' || content.expr.charAt(0) == '%') {
				content.expr = "ans" + content.expr;
			}
			bgPage.evaluate(content.expr, function(err, result) {
				if (err) {
					content.result = err;
				} else {
					$scope.contents.unshift({
						"expr": content.expr,
						"result": result
					});
					$scope.content = {};
					chrome.storage.sync.set({
						'contents': $scope.contents
					}, function() {});
				}
			})
			return true;
		}

	}
	$scope.evaluatePast = function(evt, cont) {
		if (evt.keyCode == 13 && cont.expr != '' && evt.altKey == false) {
			bgPage.evaluate(content.expr, function(err, result) {
				if (err) {
					cont.result = err;
				} else {
					cont.result = result;
					chrome.storage.sync.set({
						'contents': $scope.contents
					}, function() {});
				}
			})
			return true;
		}
	}

	$scope.clearBoard = function() {
		$scope.content = {};
		$scope.contents = [];
		bgPage.scope = {};
		bgPage.setScope();
		chrome.storage.sync.set({
			'contents': []
		}, function() {});
	}
	chrome.storage.onChanged.addListener(function(changes, namespace) {
		for (key in changes) {
			if (key == 'contents') {
				$scope.contents = changes[key].newValue;
				$scope.$apply();
			}
		}
	});
}