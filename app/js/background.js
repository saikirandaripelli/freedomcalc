var scope = {};

chrome.app.runtime.onLaunched.addListener(function() {
	chrome.app.window.create('index.html', {
		'bounds': {
			'width': 400,
			'height': 300
		}
	});
});
chrome.storage.sync.get('scope', function(result) {
	if (result.scope) {
		scope = result.scope;
	} else {
		chrome.storage.sync.set({
			'scope': scope
		}, function() {

		});
	}
});
chrome.storage.onChanged.addListener(function(changes, namespace) {
	for (key in changes) {
		if (key == 'scope') {
			scope = changes[key].newValue;
		}
	}
});
var setScope = function() {
	chrome.storage.sync.set({
		'scope': scope
	}, function() {});
}

var evaluate = function(expr, callback) {
	try {
		math.eval(expr, scope);
		result = scope.ans;
		if (typeof result === 'object') {
			if (result.value) {
				result = result.value
				if (result.unit) {
					result = result + " " + result.unit.name;
				}
			} else if (result._data) {
				result = result._data;
			} else {
				result = ''
			}
		}
		if (typeof result === 'function') {
			result = '';
		}
		setScope();
		callback(undefined, result);
	} catch (err) {
		callback(err.message);
	}

}